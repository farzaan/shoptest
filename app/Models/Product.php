<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    //as simplest as possible
    protected $fillable = [
        'title', 'description', 'provider_id', 'visible', 'commentable', 'votable', 'public_votes'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];
}
