<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use App\Repository\UserRepositoryInterface;

class UserController extends Controller
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {

        $this->userRepository = $userRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function isVotable()
    {
        $isVotable = $this->userRepository->isUserVotable();

        if ($isVotable["success"]){
            return response()->json([
                'data' => $isVotable["data"],
                'status' => 200,
                'message' => null,
            ], 200);

        } else {
            return response()->json([
                'data' => $isVotable["error"],
                'status' => 400,
                'message' => $isVotable["message"],
            ], 400);
        }
    }

}
