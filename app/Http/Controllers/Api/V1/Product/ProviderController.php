<?php

namespace App\Http\Controllers\Api\V1\Product;

use App\Http\Controllers\Controller;
use App\Repository\ProviderRepositoryInterface;
use Illuminate\Http\Request;

class ProviderController extends Controller
{
    /**
     * @var ProviderRepositoryInterface
     */
    private $providerRepository;

    public function __construct(ProviderRepositoryInterface $providerRepository)
    {
        $this->providerRepository = $providerRepository;
    }
}
