<?php

namespace App\Http\Controllers\Api\V1\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\GetLastCommentsRequest;
use App\Repository\CommentRepositoryInterface;

class CommentController extends Controller
{
    /**
     * @var CommentRepositoryInterface
     */
    private $commentRepository;

    public function __construct(CommentRepositoryInterface $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * @param GetLastCommentsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function commentList(GetLastCommentsRequest $request)
    {

        $validatedData = $request->validated();


        $listCount = isset($validatedData["count"]) && $validatedData["count"] != null ? $validatedData["count"] : 3;
        $productId = $validatedData["product_id"];

        $list = $this->commentRepository->getCommentList($productId, $listCount);

        if ($list["success"]){
            return response()->json([
                'data' => $list["data"],
                'status' => 200,
                'message' => null,
            ], 200);

        } else {
            return response()->json([
                'data' => $list["error"],
                'status' => 400,
                'message' => $list["message"],
            ], 400);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function count($id)
    {
        $count = $this->commentRepository->productCommentCount($id);

        if ($count["success"]){
            return response()->json([
                'data' => $count["data"],
                'status' => 200,
                'message' => null,
            ], 200);

        } else {
            return response()->json([
                'data' => $count["error"],
                'status' => 400,
                'message' => $count["message"],
            ], 400);
        }
    }

    /**
     * @param GetLastCommentsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function voteList(GetLastCommentsRequest $request)
    {

        $validatedData = $request->validated();


        $listCount = isset($validatedData["count"]) && $validatedData["count"] != null ? $validatedData["count"] : 3;
        $productId = $validatedData["product_id"];

        $list = $this->commentRepository->getVoteList($productId, $listCount);

        if ($list["success"]){
            return response()->json([
                'data' => $list["data"],
                'status' => 200,
                'message' => null,
            ], 200);

        } else {
            return response()->json([
                'data' => $list["error"],
                'status' => 400,
                'message' => $list["message"],
            ], 400);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function meanVotes($id)
    {
        $mean = $this->commentRepository->productMeanVotes($id);

        if ($mean["success"]){
            return response()->json([
                'data' => $mean["data"],
                'status' => 200,
                'message' => null,
            ], 200);

        } else {
            return response()->json([
                'data' => $mean["error"],
                'status' => 400,
                'message' => $mean["message"],
            ], 400);
        }
    }

}
