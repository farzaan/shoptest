<?php

namespace App\Http\Controllers\Api\V1\Product;

use App\Http\Controllers\Controller;
use App\Repository\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function get($id)
    {
        $product = $this->productRepository->getProductById($id);

        if ($product["success"]){
            return response()->json([
                'data' => $product["data"],
                'status' => 200,
                'message' => null,
            ], 200);

        } else {
            return response()->json([
                'data' => $product["error"],
                'status' => 400,
                'message' => $product["message"],
            ], 400);
        }
    }


}
