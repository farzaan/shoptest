<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class GetLastCommentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        try {

            return [
                "product_id" => "required|numeric",
                "count" => "numeric",
            ];
        }catch (\Exception $e){
            Log::info("error occurred @ " . __METHOD__);
            Log::info($e);
        }
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'product_id.required' => 'Id of Product is Required!',
            'product_id.numeric' => 'Wrong Id Format!',
            'count.numeric' => 'Wrong Count Format!',
        ];
    }


    /**
     * @param Validator $validator
     * @throws ValidationException
     */
    public function failedValidation(Validator $validator)
    {

        $response = response([
            'data' => [],
            'status'=> 422,
            'message' => 'Error in Fetching Last Comments!',
            'errors'=> $validator->errors()
        ],422);

        throw (new ValidationException($validator, $response));
    }
}
