<?php
/**
 * Created by PhpStorm.
 * User: farzan
 * Date: 9/21/2021
 * Time: 11:59 PM
 */

namespace App\Repository;


interface CommentRepositoryInterface
{
    public function getCommentList($productId, $listCount = null);
    public function productCommentCount($productId);
    public function getVoteList($productId, $listCount = null);
    public function productMeanVotes($productId);
}