<?php
/**
 * Created by PhpStorm.
 * User: farzan
 * Date: 9/21/2021
 * Time: 11:59 PM
 */

namespace App\Repository\Eloquent;


use App\Models\Comment;
use App\Repository\CommentRepositoryInterface;
use Illuminate\Support\Facades\Log;

class CommentRepository extends BaseRepository implements CommentRepositoryInterface
{
    /**
     * @var Comment
     */
    private $model;

    public function __construct(Comment $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function getCommentList($productId, $listCount = null)
    {
        try {

            $comments = $this->model->leftJoin("users", function ($q){
                $q->on("users.id", "comments.user_id");
            });

            $comments = $comments->where("accept_status", config("app.accepted_status")); // just getting accepted comments

            $comments = $comments->where("product_id", $productId);

            if($listCount){
                $comments = $comments->take($listCount);
            }

            $comments = $comments->selectRaw("comments.comment, users.first_name, comments.vote");

            $comments = $comments->orderByRaw("comments.id DESC"); //latest first

            $comments = $comments->get();

            $comments = $comments ? $comments->toArray() : [];


            return ["success" => true, "data" => $comments];
        }catch (\Exception $e){

            Log::info("Error Occurred @ " . __METHOD__);
            Log::info($e);
            return ["success" => false, "error" => $e->getMessage(), "message" => "Something Goes Wrong! Try Again Later!"];
        }
    }

    public function productCommentCount($productId)
    {
        try {

            $count = $this->model->where("accept_status", config("app.accepted_status")); // just getting accepted comments

            $count = $count->where("product_id", $productId);

            $count = $count->selectRaw("count(comments.id) as count");

            $count = $count->groupBy("comments.product_id"); //latest first

            $count = $count->first();

            $count = $count ? $count["count"] : 0;

            return ["success" => true, "data" => $count];
        }catch (\Exception $e){

            Log::info("Error Occurred @ " . __METHOD__);
            Log::info($e);
            return ["success" => false, "error" => $e->getMessage(), "message" => "Something Goes Wrong! Try Again Later!"];
        }
    }


    public function getVoteList($productId, $listCount = null, $userId = null)
    {
        try {

            $votes = $this->model->leftJoin("users", function ($q){
                $q->on("users.id", "comments.user_id");
            });

            $votes = $votes->where("accept_status", config("app.accepted_status")); // just getting accepted comments

            $votes = $votes->where("product_id", $productId);

            if($userId){
                $votes = $votes->where("comments.user_id", $userId);
            }

            if($listCount){
                $votes = $votes->take($listCount);
            }

            $votes = $votes->selectRaw("comments.vote, comments.product_id, users.first_name");

            $votes = $votes->orderByRaw("comments.id DESC"); //latest first

            $votes = $votes->get();

            $votes = $votes ? $votes->toArray() : [];


            return ["success" => true, "data" => $votes];
        }catch (\Exception $e){

            Log::info("Error Occurred @ " . __METHOD__);
            Log::info($e);
            return ["success" => false, "error" => $e->getMessage(), "message" => "Something Goes Wrong! Try Again Later!"];
        }
    }

    public function productMeanVotes($productId)
    {
        try {

            $mean = $this->model->where("accept_status", config("app.accepted_status")); // just getting accepted comments

            $mean = $mean->where("product_id", $productId);

            $mean = $mean->selectRaw("sum(comments.vote)/count(comments.id) as mean, comments.product_id");

            $mean = $mean->groupBy("comments.product_id"); //latest first

            $mean = $mean->first();

            $mean = $mean ? $mean["mean"] : 0;

            return ["success" => true, "data" => $mean];
        }catch (\Exception $e){

            Log::info("Error Occurred @ " . __METHOD__);
            Log::info($e);
            return ["success" => false, "error" => $e->getMessage(), "message" => "Something Goes Wrong! Try Again Later!"];
        }
    }

}