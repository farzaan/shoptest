<?php
/**
 * Created by PhpStorm.
 * User: farzan
 * Date: 9/21/2021
 * Time: 11:00 PM
 */

namespace App\Repository\Eloquent;


use App\Models\User;
use App\Repository\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * @var User
     */
    private $model;

    public function __construct(User $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function isUserVotable()
    {
        try {
            // to get authenticated user we'll need Auth Middleware (else this method doesn't work)
            //$authenticatedUser = Auth::user();
            $authenticatedUser = $this->model->first();

            return ["success" => true, "data" => $authenticatedUser["votable"]];
        }catch (\Exception $e){
            Log::info("Error Occurred @ " . __METHOD__);
            Log::info($e);
            return ["success" => false, "error" => $e->getMessage(), "message" => "Something Goes Wrong! Try Again Later!"];
        }
    }

}