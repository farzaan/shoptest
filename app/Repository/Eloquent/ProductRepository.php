<?php
/**
 * Created by PhpStorm.
 * User: farzan
 * Date: 9/21/2021
 * Time: 11:00 PM
 */

namespace App\Repository\Eloquent;


use App\Models\Product;
use App\Repository\CommentRepositoryInterface;
use App\Repository\ProductRepositoryInterface;
use Illuminate\Support\Facades\Log;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    /**
     * @var Product
     */
    private $model;
    /**
     * @var CommentRepositoryInterface
     */
    private $commentRepository;

    public function __construct(Product $model, CommentRepositoryInterface $commentRepository)
    {
        parent::__construct($model);
        $this->model = $model;
        $this->commentRepository = $commentRepository;
    }


    public function getProductById($productId)
    {

        try {

            $product = $this->model->where("visible", 1); // just getting accepted comments

            $product = $product->where("id", $productId);

            $product = $product->selectRaw("products.commentable, products.title, products.description,
            products.votable, products.public_votes, products.provider_id");
            //if we had providers table, we could leftJoin products with it to get provider's name

            $product = $product->first();

            if (!$product){
                return ["success" => false, "error" => "no such a product!", "message" => "No Product Found With This Information"];
            }

            //get last product votes
            $voteMean = $this->commentRepository->productMeanVotes($productId);
            $comments = $this->commentRepository->getCommentList($productId, 3);

            $result["product"] = $product;


            $result["vote_mean"] = $voteMean["success"] ? $voteMean["data"] : 0;
            $result["comments"] = $comments["success"] ? $comments["data"] : [];

            return ["success" => true, "data" => $result];
        }catch (\Exception $e){

            Log::info("Error Occurred @ " . __METHOD__);
            Log::info($e);
            return ["success" => false, "error" => $e->getMessage(), "message" => "Something Goes Wrong! Try Again Later!"];
        }

    }


}