<?php
/**
 * Created by PhpStorm.
 * User: farzan
 * Date: 9/21/2021
 * Time: 11:49 PM
 */

namespace App\Repository\Eloquent;


use App\Repository\EloquentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements EloquentRepositoryInterface
{
    /**
     * @var Model
     */
    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    // get all records of model
    public function all()
    {
        return $this->model->all();
    }

    // get last inserted record's id
    public function lastId()
    {
        return $this->model->withTrashed()->max('id');
    }

    // create a new record in the database
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    // update record in the database
    public function update(array $data, $id)
    {
        $record = $this->find($id);
        $record->update($data);
        return $record;
    }

    // find record in the database
    public function find($id){
        return $record = $this->model->find($id);
    }

    // remove record from the database
    public function destroy($id)
    {
        return $this->model->destroy($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }


    public function findWithCondition($conditions)
    {
        return $this->model->where($conditions);
    }

    public function findWhereIn($field,$value)
    {
        return $this->model->whereIn($field,$value)->get();
    }

}