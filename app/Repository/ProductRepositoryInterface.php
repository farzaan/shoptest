<?php
/**
 * Created by PhpStorm.
 * User: farzan
 * Date: 9/21/2021
 * Time: 11:57 PM
 */

namespace App\Repository;


interface ProductRepositoryInterface
{
    public function getProductById($productId);
}