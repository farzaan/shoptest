<?php
/**
 * Created by PhpStorm.
 * User: farzan
 * Date: 9/21/2021
 * Time: 11:58 PM
 */

namespace App\Repository;


interface UserRepositoryInterface
{
    public function isUserVotable();
}