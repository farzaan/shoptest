<?php
/**
 * Created by PhpStorm.
 * User: farzan
 * Date: 9/21/2021
 * Time: 11:48 PM
 */

namespace App\Repository;


interface EloquentRepositoryInterface
{
    public function all();
    public function lastId();
    public function create(array $data);
    public function update(array $data, $id);
    public function find($id);
    public function getModel();
    public function setModel($model);
    public function with($relations);
    public function findWithCondition($conditions);
    public function findWhereIn($field,$value);
}