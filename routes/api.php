<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\User\UserController;
use App\Http\Controllers\Api\V1\Product\CommentController;
use App\Http\Controllers\Api\V1\Product\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


//------------------------ User Routes -----------------------
Route::middleware([])->namespace('Api\V1\User')->prefix('/v1/user')->group(function () {
    Route::get('/votable', [UserController::class, 'isVotable'])->name('is-user-votable');
});


//------------------------ Product Routes -----------------------
Route::middleware([])->namespace('Api\V1\Product')->prefix('/v1/product')->group(function () {

    Route::post('/comment-list', [CommentController::class, 'commentList'])->name('get-list-of-comments');
    Route::get('/comment-count/{id}', [CommentController::class, 'count'])->name('get-count-of-comments');
    Route::post('/vote-list', [CommentController::class, 'voteList'])->name('get-list-of-votes');
    Route::get('/votes-mean/{id}', [CommentController::class, 'meanVotes'])->name('get-mean-of-votes');

    Route::get('/{id}', [ProductController::class, 'get'])->name('get-single-product-by-id');

});

