<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Product::count()) {
            DB::table('products')->delete();
        }

        $this->createProducts();
    }

    public function createProducts()
    {

        Product::updateOrCreate(
            ['id' => 1],
            ['id' => 1, 'title' => 'title1', 'description' => 'description1',
                'provider_id' => 1, 'visible'=> 1, 'commentable' => 1, 'votable' => 1, 'public_votes' =>1]
        );


        Product::updateOrCreate(
            ['id' => 2],
            ['id' => 2, 'title' => 'title2', 'description' => 'description2',
                'provider_id' => 1, 'visible'=> 0, 'commentable' => 1, 'votable' => 1, 'public_votes' =>0]
        );

    }

}
