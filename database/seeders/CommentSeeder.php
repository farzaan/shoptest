<?php

namespace Database\Seeders;

use App\Models\Comment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Comment::count()) {
            DB::table('comments')->delete();
        }

        $this->createComments();
    }

    public function createComments()
    {

        Comment::updateOrCreate(
            ['id' => 1],
            ['id' => 1, 'product_id' => 1, 'user_id' => 1,
                'accept_status' => 1, 'accepted_at'=> date("Y-m-d H:i:s"),
                'comment' => "comment1" , "vote" => 1]
        );

        Comment::updateOrCreate(
            ['id' => 2],
            ['id' => 2, 'product_id' => 1, 'user_id' => 1,
                'accept_status' => 2, 'accepted_at'=> date("Y-m-d H:i:s"),
                'comment' => "comment2" , "vote" => 2]
        );

        Comment::updateOrCreate(
            ['id' => 3],
            ['id' => 3, 'product_id' => 1, 'user_id' => 1,
                'accept_status' => 1, 'accepted_at'=> date("Y-m-d H:i:s"),
                'comment' => "comment3" , "vote" => 5]
        );

        Comment::updateOrCreate(
            ['id' => 4],
            ['id' => 4, 'product_id' => 1, 'user_id' => 1,
                'accept_status' => 1, 'accepted_at'=> date("Y-m-d H:i:s"),
                'comment' => "comment4" , "vote" => 3]
        );

}


}
