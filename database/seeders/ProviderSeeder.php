<?php

namespace Database\Seeders;

use App\Models\Provider;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProviderSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Provider::count()) {
            DB::table('providers')->delete();
        }

        $this->createProviders();
    }

    public function createProviders()
    {

        Provider::updateOrCreate(
            ['id' => 1],
            ['id' => 1, 'name' => 'provider1']
        );

        Provider::updateOrCreate(
            ['id' => 2],
            ['id' => 2, 'name' => 'provider2']
        );

    }
}
