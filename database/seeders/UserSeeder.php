<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (User::count()) {
            DB::table('users')->delete();
        }

        $this->createUsers();
    }

    public function createUsers()
    {

        User::updateOrCreate(
            ['id' => 1],
            ['id' => 1, 'name' => 'user1', 'email' => 'far@zaan.com', 'first_name' => 'agha',
                'last_name' => 'farzaan', 'password' => bcrypt('12345'), 'privilege' => 2, 'votable' => 1]
        );

    }
}